
import numpy as np
import matplotlib.pyplot as plt

class EjemploBorracho:
    
    def __init__(self, pasos, pos=[0,0]):
        self.pasos = pasos
        self.pos = pos
    
    
    def Trayectoria(self):
        x = []
        y = []
        plt.plot(self.pos[0],self.pos[1],"ro")
        x.append(self.pos[0])
        y.append(self.pos[1])
        
        for i in range(self.pasos):
            dir = np.random.randint(0,100)
            
            if 0<=dir<25:
                self.pos[0] += 1
                self.pos[1] += 0
                
            elif 25<=dir<50:
                
                self.pos[0] += 0
                self.pos[1] += 1
            
            elif 50<=dir<75:
                
                self.pos[0] += -1
                self.pos[1] += 0
                
            elif 75<=dir<100:
                
                self.pos[0] += 0
                self.pos[1] += -1


            x.append(self.pos[0])
            y.append(self.pos[1])
        
        
    
        plt.plot(x,y,"--") 
        plt.plot(x[-1],y[-1],"go")   
        plt.show()
            

                
            