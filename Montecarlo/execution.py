from Montecarlo import Monte_Carlo


if __name__=='__main__':
    
    M = Monte_Carlo(100000)
    pi_calcu = M.calcular_pi()
    
    print('El pi calculado: {}'.format(pi_calcu))