
import numpy as np


class Monte_Carlo:
    """
    Calcular pi mediante método de montecarlo

    Parameters
    
    - N: Número de puntos aleatorios
    
    Return
    
    Número pi calculado por método de montecarlo

    """
    def __init__(self, N):
        
        self.N = N
        self.x = np.random.random(self.N)*2 - 1
        self.y = np.random.random(self.N)*2 - 1
        self.n_aciertos = 0
        
    def calcular_pi(self):
        
        for i in range(self.N):
            if (self.x[i]**2+self.y[i]**2)<=1:
                self.n_aciertos += 1                
        pi = 4*self.n_aciertos/self.N    
        
        return pi