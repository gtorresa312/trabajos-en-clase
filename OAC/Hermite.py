from sympy import *


class hermitee:
    
    def __init__(self, n, z):
        self.n = n
        self.z = z
        
    def solve_equation(self):
        return ((-1)**self.n)*exp(self.z**2)*diff(exp(-self.z**2),self.z,self.n)
        
        
        
    
    