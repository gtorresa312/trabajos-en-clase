from Hermite import hermitee
from sympy import *
import matplotlib.pyplot as plt
import numpy as np
import json


class quantum_harmonic_oscilator:
    

    def __init__(self):
        
        # Parámetros
        self.hbar = 1
        self.m = 1
        self.k = 1
        self.w = np.sqrt(self.k/self.m)
        self.a = np.sqrt((self.m*self.w)/self.hbar)
        

        self.z = symbols('z')
        self.sols = []
        with open('config.json','r') as file:
            date = json.load(file)
        self.n = date["n"]
        self.colores = date["color"]
        self.legends = date["legend"]
        
        
    def sol_psi(self):
        for i in self.n:
            N = 1 / sqrt((2**i) * sqrt(pi) * factorial(i))
            H = hermitee(i,self.z)
            psi2 = N * H.solve_equation() * exp((-self.z**2)/2)
            self.sols.append(lambdify(self.z,psi2**2,'numpy'))
             
        
    def plot_psi(self):
        
        self.sol_psi()
        x = np.linspace(-3,3,500)*self.a
        y = [sol(x) for sol in self.sols]
        V = 0.5*self.m*self.w**2*x**2
        
        for i in range(len(self.n)):
            plt.plot(x,y[i]+(i + 0.5)*self.hbar*self.w,color = self.colores[i], label = self.legends[i])
            plt.hlines((i + 0.5)*self.hbar,-3,3,linestyles='--',color =self.colores[i])
        
        plt.plot(x,0.5*self.k*x**2)
        plt.plot()
        plt.legend()
        plt.xlabel('x')
        plt.ylabel('$\psi(x)$')
        plt.show()
        
        
        
        
        
        

    

