from MonteCarlo import Monte_Carlo
import numpy as np

if __name__ == '__main__':
    
    f = lambda x: x**2
    a = 0
    b = 2
    n = 10000
    
    sol = Monte_Carlo(f,n,a,b)
    print(sol.Integrate1())
    print(sol.Integrate2())