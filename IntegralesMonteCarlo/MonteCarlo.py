import numpy as np 
class Monte_Carlo:
    
    def __init__(self, f, n, a, b):
      self.f = f
      self.n = n
      self.a = a
      self.b = b
      
      
    def Integrate1(self):
        
        sum = 0
        
        for i in range(self.n):
            x = np.random.uniform(self.a,self.b)
            sum += (self.f(x))

        return ((self.b-self.a)/self.n)*sum
    
    def Integrate2(self):
        x = np.linspace(self.a,self.b,1000)
        
        f_max = max(self.f(x))
        A = (self.b-self.a)*f_max
        count = 0
        for i in range(self.n):
            xi = np.random.uniform(self.a,self.b)
            yi = np.random.uniform(0,f_max)
            
            if 0<yi<self.f(xi):
                count += 1
            
        return (count/self.n)*A
    
    
        
        
        
        