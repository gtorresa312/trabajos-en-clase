import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp

class EDO:
    
    """
    Esta clase permite solucionar ecuaciones diferenciales ordinarias
    de segundo orden desacopladas
    
    Args:
        a: Límite inferior del intérvalo
        b: Límite superior del intérvalo
        n: Número de divisiones
        y_0: Condición inicial para y
        f: función que contiene las ecuaciones diferenciales
        desacopladas 

        
    Returns
        Retorna los arreglos y(t) y y'(t) como la solución de la
        EDO, usando distintos métodos (Solución analítica, Euler y RK4).
        Además retorna la gráfica t vs y(t) de la solución obtenida por cada 
        uno de los métodos
    """
    def __init__(self,a,b,N,y_0,f):
        self.a=a
        self.b=b 
        self.N=N
        self.y_0=y_0
        self.t=[]
        self.f=f


    def h(self):  #Salto entre puntos contiguos
        return (self.b-self.a)/self.N
    
    
    def T(self):   #Arreglo de números para los valores de x
        self.t = np.arange(self.a,self.b+self.h(),self.h())

        
    def Euler(self):
        self.T()
        y = self.y_0.copy()
        n = len(y)
        sol_euler = np.zeros(n*len(self.t)).reshape(n,len(self.t))
        for j in range(len(self.t)):
            func = self.f(self.t,y)
            for i in range(n):
                y[i] += self.h() * func[i]
                sol_euler[i,j] = y[i]
        return sol_euler

    def RK4(self):
        h2 = self.h()/2
        y = self.y_0.copy()
        n = len(y)
        yt = [0]*(n)
        
        sol_Rk4 = np.zeros(n*len(self.t)).reshape(n,len(self.t))
        for j in range(len(self.t)):
            k1 = self.f(self.t,y) 
            for i in range(n): 
                yt[i] = y[i] + h2*k1[i]
            k2 = self.f(self.t+h2,yt) 
            for i in range(n): 
                yt[i] = y[i] + h2*k2[i]
            k3 = self.f(self.t+h2,yt) 
            for i in range(n): 
                yt[i] = y[i] + self.h() *k3[i]
            k4 = self.f(self.t+self.h(),yt)
            
            h6 = self.h()/6
            
            for i in range(n): 
                y[i] += h6*(k1[i] + 2*(k2[i] + k3[i]) + k4[i])
                sol_Rk4[i,j]=y[i]
        return sol_Rk4
    
    def solanalitica(self):   #Solución analítica
        self.T()
        y = self.y_0.copy()
        sol_anal = solve_ivp(self.f, [self.a,self.b], y, t_eval=self.t)
        return sol_anal.y
    
    def figPlot(self):   #Grafica los resultados
        
        self.T()
        plt.figure(figsize=(10,5))
        theuler, dtheuler= self.Euler()
        yanal, dyanal = self.solanalitica()
        yrk4, dyrk4 = self.RK4()
        plt.plot(self.t, dtheuler, label="Solución Euler")
        plt.plot(self.t, dyanal, label='Solucion analítica')
        plt.plot(self.t, dyrk4, label='Solucion RK4')
        plt.xlabel('t')
        plt.ylabel('theta')
        plt.title('Solución de la EDO (Diagrama de fase)')
        plt.legend()
        plt.show()