from EDO_sol import EDO


if __name__ == '__main__':
    
    a = 0   # Limite inferior del intervalo
    b = 5   # Limite superior del intervalo
    n = 1000   # Número de divisiones
    y0 = [0,1]    # Condición inicial
    g = 9.81
    l = 1
    gamma =0.4
    
    def Func(t,y):
        th, v = y
        return [v,-(g/l)*th - 2*gamma*v]

    
    Solver = EDO(a,b,n,y0,Func)
    Solver.figPlot()
    






    
