from masa_acoplada import masa_girando, masa_girandoT2

if __name__=='__main__':
    
    masa = masa_girando()
    masa2 = masa_girandoT2()
    
    print('w = {}'.format(masa.w()))
    print('T2 = {}'.format(masa.T_faltante()))
    
    print('w = {}'.format(masa2.w()))
    print('T1 = {}'.format(masa2.T_faltante()))