import numpy as np

class masa_girando:

    def __init__(self, T1 = 250 ,l = 2.4, s = 2, m = 6):
        self.l = l
        self.s = s
        self.m = m
        self.T1 = T1
        self.g = 9.81
        
        
    def r(self):
        return self.s**2-(self.l/2)**2
        

    def cos(self):
        return self.r()/self.s
    
    def sen(self):
        return (self.l/2)/self.s
    
    def T_faltante(self):
        self.T2 = self.T1 - (self.m*self.g/self.sen())
        return self.T2
            
    def w(self):
        self.T_faltante()
        return np.sqrt(((self.T1+self.T2)*self.cos())/(self.m*self.r()))
            
        
class masa_girandoT2(masa_girando):
    
    
    def __init__(self, T2 = 151.89999999999998, l = 2.4, s = 2, m = 6 ):
        super().__init__(l = 2.4, s = 2, m = 6)
        self.T2 = T2

    
    def T_faltante(self):
        self.T1 = self.T2 + (self.m*self.g/self.sen())
        return self.T1
            
    
        
    
    