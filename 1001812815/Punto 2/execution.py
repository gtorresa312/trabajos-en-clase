from EDO_sol import EDO
import numpy as np


if __name__=="__main__":
    
    a = 0   # Limite inferior del intervalo
    b = 1   # Limite superior del intervalo
    n = 100   # Número de divisiones
    y0 = -1    # Condición inicial
    f = lambda x,y: np.exp(2*x)+3  #función de la ecuación diferencial (dy/dx = f(x,y))
    punto = 4  #Para acotar el intérvalo a un punto en específico
    
    Solver = EDO(a,b,n,y0,f,punto)
    Solver.figPlot()
    
    #Solucionando el Oscilador armónico
    

    
