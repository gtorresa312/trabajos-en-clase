import numpy as np

class pendulo_balistic:
    
    def __init__(self, M, m,g=9.81, h = False,V_bala = False):
        self.g = g
        self.h = h
        self.V_bala = V_bala/3.6
        self.M = M
        self.m = m

        
    def gravedad(self):
        return self.g    

    def altura(self):
        if self.V_bala:
            self.h = (1/2*self.g)*(self.V_bala*self.m/(self.m + self.M))**2
        return self.h
    
    def V_init(self):
        if self.h:
            self.V_bala = (1 + self.M/self.m)*np.sqrt(2*self.g*self.h)
        return self.V_bala
        
class V_min(pendulo_balistic):
    
    def __init__(self, M, m, l, g=9.81, h = False,V_bala = False):
        pendulo_balistic.__init__(self,m,M,l,h,V_bala)
        self.l = l

        
    def  V(self):
        return np.sqrt(5*self.gravedad()*self.l)   
        