import numpy as np

class Prob_colision:

    def __init__(self,N,r=10,R=100):

        self.r = r
        self.R = R
        self.N = N



    def crear_particula(self):
        x = np.random.uniform(-(self.R-self.r),self.R-self.r)
        y = np.random.uniform(-(self.R-self.r),self.R-self.r)

        if x**2+y**2<=(self.R-self.r)**2:
            return x,y



    def colision(self):
        x1,y1 = self.crear_particula()
        x2,y2 = self.crear_particula()
        
        if (x1-x2)**2+(y1-y2)**2 <= (2*self.r)**2:
            return 1
        else:
            return 0

    def Prob_colision(self):

        num_colision = 0
        for i in range(self.N):
            num_colision += self.colision()

        return (num_colision/self.N)*100

            


